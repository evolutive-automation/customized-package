import fs from 'fs'
import path from 'path'
import { app } from 'electron'
import { MachineConfig } from '@/types/config'

const getConfigFromFile = (): Promise<MachineConfig> => {
  return new Promise((resolve, reject) => {
    // let configPath = path.join(
    //   app.getPath("home"),
    //   "machine-automation.config"
    // );
    // if (process.env.NODE_ENV !== "production") {
    //   configPath = path.join(__dirname, "..", "machine-automation.config");
    // }
    // if (!fs.existsSync(configPath)) {
    //   reject("設定檔不存在，請聯絡RD");
    // }
    // const buffer: Buffer = fs.readFileSync(configPath);
    // const configData = JSON.parse(buffer.toString());

    const configData = {
      machineNo: '',
      executionEnvironment: 'production',
      downloadDir: path.join(app.getPath("home"), 'customized-package'),
      labelDir: path.join(app.getPath("home"), 'customized-package', 'label'),
      logDir: path.join(app.getPath("home"), 'customized-package', 'log')
    }

    // console.log(configData)
    resolve({
      'customized-package': configData
    });
  });
};

const setProcessEnv = (configData: MachineConfig) => {
  Object.entries(configData["customized-package"]).forEach(([ key, value ]) => {
    process.env[key] = value
  })
};

const checkLocalDir = () => {
  const needCheckDirs = Object.keys(process.env).filter(key => /Dir/.test(key));
  needCheckDirs.forEach(dir => {
    const dirPath = process.env[dir];
    if (dirPath && !fs.existsSync(dirPath)) {
      fs.mkdirSync(dirPath);
    }
  });
};

export const setConfig = async () => {
  const configData = await getConfigFromFile();
  setProcessEnv(configData);
  checkLocalDir();
};
