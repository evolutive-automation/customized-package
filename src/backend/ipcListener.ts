import { app, ipcMain, BrowserWindow } from 'electron'
import path from 'path'
import { log } from '@/backend/logger'
import ptp from 'pdf-to-printer'

const listenPrintPdf = () => {
  ipcMain.on("print-pdf", async (event, filePath) => {
    // console.log(filePath)
    // console.log(process.env, process.env.labelDir)
    // const labelPath = path.join(process.env.labelDir, filename);
    // log(`標籤路徑：${filePath}`, "success");

    const options = {
      win32: ['-print-settings landscape,fit']
    }

    await ptp
      .print(filePath, options)
      // .then(() => {
      //   // log(`產生成功：${filePath}`, "success");
      // })
      // .catch(err => {
      //   // log(`產生失敗：${err}`, "error");
      // });
  });
};

const listenGetConfig = (window: BrowserWindow) => {
  ipcMain.on("get-configs", () => {
    window.webContents.send("get-configs-result", JSON.stringify(process.env));
  });
};

export const registerIpcListener = (window: BrowserWindow) => {
  listenGetConfig(window);
  listenPrintPdf();
};
