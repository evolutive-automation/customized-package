import fs from 'fs'
import moment from 'moment'

const today = moment()
  .locale("zh-tw")
  .format("YYYY-MM-DD");
const successFilename = `${today}-success.log`;
const errorFilename = `${today}-error.log`;

const checkOrCreateDir = (logDir?: string) => {
  if (logDir && !fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
  }
};

export const log = (message: string, type: string) => {
  // TODO
  // hard code
  // const logDir = process.env.logDir;
  const logDir = 'D:/customized-package/log';
  checkOrCreateDir(logDir);
  const filename = type === "success" ? successFilename : errorFilename;
  fs.appendFileSync(`${logDir}/${filename}`, `${message}\r\n`);
};
