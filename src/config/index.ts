export const domain = process.env.VUE_APP_DOMAIN
export const apiToken = process.env.VUE_APP_TOKEN
export const fileServer = process.env.VUE_APP_FILE_SERVER
export const previewPath = 'preview/'
export const casePath = 'case/'
