import axios from 'axios'
import { domain, apiToken } from '../config'
import {
  GetPackingHotstampItemsResponse
} from '../types/hotstamp'
import {
  CommonResponse
} from '../types/index'

export const getPackingHotstampItems = async (
  caseType: string
): Promise<GetPackingHotstampItemsResponse> => {
  return (await axios.get(`${domain}/api/hotstamp-items/pack/${caseType}`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const setDonePack = async (
  id: number
): Promise<CommonResponse> => {
  return (await axios.get(`${domain}/api/hotstamp-items/${id}/mark-packed`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  }))
}

export const markAsReprint = async (
  id: number,
  ngType: string
): Promise<CommonResponse> => {
  return (await axios.post(`${domain}/api/hotstamp-items/${id}/mark-reprint`, {
    ng_type: ngType
  }, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const markAsNg = async (
  id: number
): Promise<CommonResponse> => {
  return (await axios.get(`${domain}/api/hotstamp-items/${id}/mark-ng`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}
