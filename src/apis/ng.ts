import axios from 'axios'
import { domain, apiToken } from '../config'
import {
  SearchDesignSortsResponse,
  SearchDevicesResponse,
  SearchColorsResponse,
  GetAllPackingPrintItemsResponse,
  PrintLabelResponse
} from '../types/ng'

export const searchDesignSorts = async (
): Promise<SearchDesignSortsResponse> => {
  return (await axios.get(`${domain}/api/design-sorts/search`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const searchDevices = async (
): Promise<SearchDevicesResponse> => {
  return (await axios.get(`${domain}/api/devices/search`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const searchColors = async (
): Promise<SearchColorsResponse> => {
  return (await axios.get(`${domain}/api/colors/search`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const getAllPackingPrintItems = async (
  status: string
): Promise<GetAllPackingPrintItemsResponse> => {
  return (await axios.get(`${domain}/api/print-items/pack/designs`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    params: {
      status
    },
    validateStatus: () => true
  })).data
}

export const reportPrintItemNg = async (
  printItemId: number,
  ngType: string
): Promise<PrintLabelResponse> => {
  return (await axios.post(`${domain}/api/print-items/${printItemId}/mark-ng`, {
    ng_type: ngType
  }, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}
