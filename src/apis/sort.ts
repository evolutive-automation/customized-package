import axios from 'axios'
import { domain, apiToken } from '../config'
import {
  getSortItemsResponse
} from '../types/sort'

export const getSortItems = async (form: { barcode: string }): Promise<getSortItemsResponse> => {
  return (await axios.post(`${domain}/api/shipment-items/scan-barcode`, form, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}
