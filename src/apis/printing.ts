import axios from 'axios'
import { domain, apiToken } from '../config'
import {
  SearchPrintersByInkTypeResponse,
  SearchDevicesResponse,
  SearchColorsResponse,
  GetBatchDataByPrinterStatusResponse
} from '../types/printing'

export const searchPrintersByInkType = async (
  inkType: string
): Promise<SearchPrintersByInkTypeResponse> => {
  return (await axios.get(`${domain}/api/printers/${inkType}`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const searchDevices = async (
): Promise<SearchDevicesResponse> => {
  return (await axios.get(`${domain}/api/devices/search`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const searchColors = async (
): Promise<SearchColorsResponse> => {
  return (await axios.get(`${domain}/api/colors/search`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const getBatchDataByPrinterStatus = async (
  status: string,
  printerId: number
): Promise<GetBatchDataByPrinterStatusResponse> => {
  return (await axios.get(`${domain}/api/batches/${status}/printers/${printerId}`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

// export const getBatchPickingList = async (
//   batchId: string
// ): Promise<GetBatchDataByPrinterStatusResponse> => {
//   return (await axios.get(`${domain}/api/batches/${batchId}/printers/generate-picking-list`, {
//     headers: {
//       'CUSTOMIZED-SERVER-API-TOKEN': apiToken
//     },
//     validateStatus: () => true
//   })).data
// }

export const getBatchPickingPDF = async (
  batchId: number
) => {
  return await axios.get(`${domain}/api/batches/${batchId}/generate-picking-list`, {
    responseType: 'blob',
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    }
  })
}
