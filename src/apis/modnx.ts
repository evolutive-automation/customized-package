import axios from 'axios'
import { domain, apiToken } from '../config'
import {
  SearchWaresResponse,
  SearchDevicesResponse,
  SearchColorsResponse,
  GetModNXPrintItemByBatchIdResponse,
  GetPrintItemByBatchIdResponse,
  SetDonePackResponse,
  ReportPrintItemNgResponse
} from '../types/modnx'

export const searchWares = async (
  isUsing: boolean,
  type: string,
  status: string
): Promise<SearchWaresResponse> => {
  return (await axios.get(`${domain}/api/wares/search`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    params: {
      type,
      is_using: isUsing,
      status
    },
    validateStatus: () => true
  })).data
}

export const searchDevices = async (
): Promise<SearchDevicesResponse> => {
  return (await axios.get(`${domain}/api/devices/search`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const searchColors = async (
): Promise<SearchColorsResponse> => {
  return (await axios.get(`${domain}/api/colors/search`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const getModNXPrintItemByBatchId = async (
  batchId: number
): Promise<GetModNXPrintItemByBatchIdResponse> => {
  return (await axios.get(`${domain}/api/print-items/pack/batches/${batchId}`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const getPrintItemByBatchId = async (
  batchId: number
): Promise<GetPrintItemByBatchIdResponse> => {
  return (await axios.get(`${domain}/api/print-items/pack/batches/${batchId}`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const printLabel = async (
  shipmentItemId: number
) => {
  return await axios.get(`${domain}/api/shipment-items/${shipmentItemId}/generate-sticker`, {
    responseType: 'arraybuffer',
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    }
  })
}

export const setDonePack = async (
  printItemId: number
): Promise<SetDonePackResponse> => {
  return (await axios.post(`${domain}/api/print-items/${printItemId}/mark-packed`, {}, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const reportPrintItemNg = async (
  itemId: number,
  ngType: string
): Promise<ReportPrintItemNgResponse> => {
  return (await axios.post(`${domain}/api/print-items/${itemId}/mark-ng`, {
    ng_type: ngType
  }, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}
