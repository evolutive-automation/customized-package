import axios from 'axios'
import { domain, apiToken } from '../config'
import {
  SearchDesignSortsResponse,
  SearchDevicesResponse,
  SearchColorsResponse,
  GetAllPackingPrintItemsResponse,
  SetDonePackResponse,
  ReportPrintItemNgResponse
} from '../types/solidsuit'

export const searchDesignSorts = async (
): Promise<SearchDesignSortsResponse> => {
  return (await axios.get(`${domain}/api/design-sorts/search`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const searchDevices = async (
): Promise<SearchDevicesResponse> => {
  return (await axios.get(`${domain}/api/devices/search`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const searchColors = async (
): Promise<SearchColorsResponse> => {
  return (await axios.get(`${domain}/api/colors/search`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const getAllPackingPrintItems = async (
  caseType: string,
  status: string
): Promise<GetAllPackingPrintItemsResponse> => {
  return (await axios.get(`${domain}/api/print-items/pack/designs`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    params: {
      caseType,
      status
    },
    validateStatus: () => true
  })).data
}

export const printLabel = async (
  shipmentItemId: number,
  type: string
) => {
  return await axios.get(`${domain}/api/shipment-items/${shipmentItemId}/generate-sticker/${type}`, {
    responseType: 'arraybuffer',
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    }
  })
}

export const setDonePack = async (
  printItemId: number
): Promise<SetDonePackResponse> => {
  return (await axios.post(`${domain}/api/print-items/${printItemId}/mark-packed`, {}, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const reportPrintItemNg = async (
  itemId: number,
  ngType: string
): Promise<ReportPrintItemNgResponse> => {
  return (await axios.post(`${domain}/api/print-items/${itemId}/mark-ng`, {
    ng_type: ngType
  }, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const getPackItems = async (usedType: string, wareNo: string)
  : Promise<GetAllPackingPrintItemsResponse> => {
  return (await axios.get(`${domain}/api/print-items/${usedType}/wares/${wareNo}`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    }
  })).data
}
