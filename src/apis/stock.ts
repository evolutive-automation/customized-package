import { CommonResponse } from '@/types'
import axios from 'axios'
import { domain, apiToken } from '../config'
import {
  GetStockPickingListResponse,
  GetStockItemsResponse,
  CancelStockItemsResponse,
  GetStockListResponse,
  Stock
} from '../types/stock'

export const getStockPickingList = async (date: string): Promise<GetStockPickingListResponse> => {
  return (await axios.get(`${domain}/api/shipment-items/stock-picking-list/${date}`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const generateStockPickingList = async (date: string): Promise<Blob> => {
  return (await axios.get(`${domain}/api/shipment-items/download-stock-picking-list/${date}`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    responseType: 'blob',
    validateStatus: () => true
  })).data
}

export const getStockItems = async (date: string, sku: string): Promise<GetStockItemsResponse> => {
  return (await axios.get(`${domain}/api/shipment-items/get-stock-item/${date}/${sku}`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const cancelStockItems = async (date: string, form: { sku: string, quantity: string }): Promise<CancelStockItemsResponse> => {
  return (await axios.post(`${domain}/api/shipment-items/cancel-fetch-stock/${date}`, form, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const generateStockStickers = async (form: { shipmentItemIds: Array<number> }): Promise<Buffer> => {
  return (await axios.post(`${domain}/api/shipment-items/geneate-stock-stickers`, form, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    responseType: 'arraybuffer',
    validateStatus: () => true
  })).data
}

export const markStockItemsPacked = async (form: { shipmentItemIds: Array<number> }): Promise<CommonResponse> => {
  return (await axios.post(`${domain}/api/shipment-items/mark-stock-items-packed`, form, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const getStockList = async (): Promise<GetStockListResponse> => {
  return (await axios.get(`${domain}/api/stocks`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const updateStock = async (form: Stock): Promise<CommonResponse> => {
  return (await axios.post(`${domain}/api/stocks`, form, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const uploadStocks = async (form: FormData): Promise<CommonResponse> => {
  return (await axios.post(`${domain}/api/stocks/upload-file`, form, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const downloadList = async (): Promise<Blob> => {
  return (await axios.get(`${domain}/api/stocks/download-list`, {
    responseType: 'blob',
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}

export const deleteStock = async (form: { type: string, sku: string }): Promise<CommonResponse> => {
  return (await axios.post(`${domain}/api/stocks/delete-stock`, form, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    },
    validateStatus: () => true
  })).data
}
