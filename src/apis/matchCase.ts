import axios from 'axios'
import { domain, apiToken } from '../config'
import {
  getMatchCaseItemsResponse
} from '../types/matchCase'

export const getMatchCaseItems = async (usedType: string, wareNo: string)
  : Promise<getMatchCaseItemsResponse> => {
  return (await axios.get(`${domain}/api/print-items/${usedType}/wares/${wareNo}`, {
    headers: {
      'CUSTOMIZED-SERVER-API-TOKEN': apiToken
    }
  })).data
}
