import {
  CaseType,
  Device,
  Color,
  DeviceCategory,
  ColorCategory,
  DesignSort,
  DoneShipmentItem
} from "./index";
import { DesignSortPrintItem } from "./solidsuit"

export interface Ware {
  id: number;
  batchId: number;
  specialTag: string;
  no: string;
  type: 'phone-box' | 'airpods-box' | 'clear-box';
  description: string;
  isUsing: boolean;
}

export interface SearchDevicesResponse {
  ok: boolean;
  data: Array<DeviceCategory>;
  message?: string;
}

export interface SearchColorsResponse {
  ok: boolean;
  data: Array<ColorCategory>;
  message?: string;
}

export interface ModNXPrintItem {
  id: number,
  shipmentItemId: number,
  group: number,
  orderTag: string,
  laserType: string,
  isNg: boolean,

  packImages: Array<string>,
  stickers: Array<object>,

  sku: string,

  displayTitle: string,
  imagePath: string,

  note: string,
  packedAt: string,

  designSort: DesignSort,
  caseType: CaseType,
  device: Device,
  color: Color
}

export interface ModNXPackGroup {
  group: number,
  caseTypeCode: string,
  pack: string,
  orderTag: string,
  cardboard: string,
  clamshell: string,
  film: string,
  shape: string,
  laserType: string,
  stickers: Array<object>,
  packImages: Array<string>,
  printItems: Array<ModNXPrintItem>
}

export interface SearchWaresResponse {
  ok: boolean;
  data: Array<Ware>;
  message?: string;
}

export interface GetModNXPrintItemByBatchIdResponse {
  ok: boolean;
  data: Array<ModNXPrintItem>;
  message?: string;
}

export interface GetPrintItemByBatchIdResponse {
  ok: boolean;
  data: Array<DesignSortPrintItem>;
  message?: string;
}

export interface PrintLabelResponse {
  ok: boolean;
  data: {
    // TODO
    printItemId: number;
    packedAt: string;
  };
  message?: string;
}

export interface SetDonePackResponse {
  ok: boolean;
  data: {
    id: number;
    batchId: number;
    group: number;
    sequence: number;
    printedAt: string;
    packedAt: string;
    shipmentItem: DoneShipmentItem;
  };
  message?: string;
}

export interface ReportPrintItemNgResponse {
  ok: boolean;
  message?: string;
}
