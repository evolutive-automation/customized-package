import { CaseType, Color, Device } from './index'

export interface HotstampItem {
  id: number
  shipmentItemId: number
  textLength: number
  text: string
  font: string
  color: string
  hasNg: boolean
  pastNg: boolean
  orderTag: string
  sku: string
  imagePath: string
  note: string
  packedAt: string
  isReprint: boolean
  packImages: Array<string>
  caseType: CaseType
  device: Device
  caseColor: Color
  createDate: string
}

export interface PackGroup {
  orderTag: string,
  caseTypeCode: string,
  pack: string,
  film: string,
  shape: string,
  clamshell: string,
  cardboard: string,
  packImages: Array<string>,
  hotstampItems: Array<HotstampItem>
}

export interface GetPackingHotstampItemsResponse {
  ok: boolean
  data: Array<HotstampItem>
  message?: string
}
