import { CaseType, Color, Device, Sticker } from './index'

export interface Stock {
  orderTag?: string,
  type?: string,
  typeText?: string,
  imagePath?: string,
  casePath?: string,
  sku: string
  quantity: number
}

export interface StockItem {
  designSort: {
    type: string,
    name: string
  },
  id: number,
  shipmentId: number,
  group: number,
  displayTitle: string,
  imagePath: string,

  isNg: boolean,
  hasNg: boolean,
  createDate: string,

  packImages: Array<string>,
  stickers: Array<Sticker>,

  laserType: string,
  note: string,
  orderTag: string,
  packedAt: string,
  sku: string,
  stockSku: string,
  caseType: CaseType,
  color: Color,
  device: Device
}

export interface ShipmentItem {
  id: number
  shipmentId: number
  status: string
  isCustomized: boolean
  isNg: boolean
  sku: string
  stockSku: string
  productTitle: string
  displayTitle: string
  caseSku: string
  caseTypeCode: string
  brandCode: string
  deviceCode: string
  colorCode: string
  imagePath: string
  quantity: number
  inStockQuantity: number
  printedQuantity: number
  packedQuantity: number
  hottedQuantity: number
  ngQuantity: number
}

export interface GetStockPickingListResponse {
  ok: boolean
  data: Array<Stock>
  message?: string
}

export interface GetStockItemsResponse {
  ok: boolean
  data: {
    notHotstampTotal: number
    hotstampTotal: number
    items: Array<StockItem>
  }
  message?: string
}

export interface CancelStockItemsResponse {
  ok: boolean
  data: Array<ShipmentItem>
  message?: string
}

export interface GetStockListResponse {
  ok: boolean
  data: Array<Stock>
  message?: string
}
