import { DeviceCategory, ColorCategory, CaseType, Color, Device, DesignSort, Printer, PrintingBatchShipmentItem } from "./index";

export interface SearchPrintersByInkTypeResponse {
  ok: boolean;
  data: Array<Printer>;
  message?: string;
}

export interface ShipmentItem {
  hasNg: boolean
  device: Device,
  color: Color
}

export interface PrintingBatchPrintItem {
  id: number,
  shipmentItem: ShipmentItem
}

export interface PrintingBatch {
  id: number,
  printStartedAt: string,
  printItems: Array<PrintingBatchPrintItem>
}

export interface SearchDevicesResponse {
  ok: boolean;
  data: Array<DeviceCategory>;
  message?: string;
}

export interface SearchColorsResponse {
  ok: boolean;
  data: Array<ColorCategory>;
  message?: string;
}

export interface GetBatchDataByPrinterStatusResponse {
  ok: boolean;
  data: Array<PrintingBatch>;
  message?: string;
}

export interface PrintingGroup {
  batchId: number,
  printerId: number,
  brandCode: string,
  deviceCode: string,
  colorCode: string,
  name: string,
  count: number
}
