export interface CustomizedPackage {
  machineNo: string;
  executionEnvironment: string;
  downloadDir: string;
  labelDir: string;
  logDir: string;
}

export interface MachineConfig {
  'customized-package': CustomizedPackage;
}
