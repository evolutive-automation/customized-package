/* eslint-disable @typescript-eslint/ban-types */
import { DeviceCategory, ColorCategory, DoneShipmentItem, CaseType, Color, Device, DesignSort } from './index'

export interface SearchDesignSortsResponse {
  ok: boolean;
  data: {
    'design-phone': Array<DesignSort>;
    'person-phone': Array<DesignSort>;
    'design-airpods': Array<DesignSort>;
  };
  message?: string;
}

export interface DesignSortGroup {
  id: string,
  name: string,
  list: Array<DesignSort>
}

export interface DesignSortPrintItem {
  designSort: {
    type: string,
    name: string
  },
  id: number,
  shipmentItemId: number,
  group: number,
  displayTitle: string,
  imagePath: string,

  isNg: boolean,
  hasNg: boolean,
  pastNg: boolean,
  createDate: string,

  packImages: Array<string>,
  stickers: Array<object>,
  laserType: string,
  note: string,
  orderTag: string,
  packedAt: string,
  sku: string,
  caseType: CaseType,
  color: Color,
  device: Device
}

export interface DesignSortPackGroup {
  designSortType: string,
  designSortName: string,
  orderTag: string,
  caseTypeCode: string,
  pack: string,
  film: string,
  shape: string,
  cardboard?: string,
  laserType: string,
  stickers: Array<object>,
  packImages: Array<string>,
  printItems: Array<DesignSortPrintItem>
}

export interface SearchDevicesResponse {
  ok: boolean;
  data: Array<DeviceCategory>;
  message?: string;
}

export interface SearchColorsResponse {
  ok: boolean;
  data: Array<ColorCategory>;
  message?: string;
}

export interface GetAllPackingPrintItemsResponse {
  ok: boolean;
  data: Array<DesignSortPrintItem>;
  message?: string;
}

export interface PrintLabelResponse {
  ok: boolean;
  data: {
    printItemId: number;
    packedAt: string;
  };
  message?: string;
}

export interface SetDonePackResponse {
  ok: boolean;
  data: {
    id: number;
    batchId: number;
    group: number;
    sequence: number;
    printedAt: string;
    packedAt: string;
    shipmentItem: DoneShipmentItem;
  };
  message?: string;
}

export interface ReportPrintItemNgResponse {
  ok: boolean;
  message?: string;
}
