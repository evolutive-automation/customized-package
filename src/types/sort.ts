export interface DesignSort {
  type: string,
  name: string
}

export interface Color {
  active: boolean,
  name: string,
  chineseName: string,
  colorHex: string,
  code: string,
  originCode: string
}

export interface Device {
  brandCode: string,
  name: string,
  code: string,
  film: string,
  cardboard: string,
  shape: string,
  clamshell: string
}

export interface CaseType {
  type: string,
  code: string,
  pack: string
}

export interface Sticker {
  uuid: string,
  sku: string,
  positionName: string,
  stickerName: string,
  caseTypeCode: string,
  designType: string,
  packingNote: string,
  stickerImagePath: string,
  positionImagePath: string
}

export interface SortItem {
  id: number,
  isCancelled: boolean,
  isPaused: boolean,
  isOverseas: boolean,
  pasteSticker: boolean,
  hasNg: boolean,
  ngType: string,
  orderTag: string,
  sourceType: string,
  imagePath: string,
  note: string,
  designSort: DesignSort,
  laserType: string,
  stickers: Array<Sticker>,
  designText: string,
  packImages: Array<string>,
  caseType: CaseType,
  color: Color,
  device: Device,
  autoMovedDate: string
}

export interface getSortItemsResponse {
  ok: boolean
  data: SortItem
  message?: string
}
