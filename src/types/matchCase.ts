export interface Color {
  active: boolean,
  name: string,
  chineseName: string,
  colorHex: string,
  code: string,
  originCode: string,
}

export interface Device {
  brandCode: string,
  name: string,
  code: string,
  film: string,
  cardboard: string,
  shape: string,
  clamshell: string
}

export interface MatchCaseItem {
  id: number,
  batchId: number,
  subGroup: number,
  orderTag: string,
  wareNo: string,
  imagePath: string,
  hasNg: boolean,
  ngType: string,
  designText: string,
  color: Color,
  deivce: Device
}

export interface getMatchCaseItemsResponse {
  ok: boolean
  data: Array<MatchCaseItem>
  message?: string
}
