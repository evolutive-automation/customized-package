export interface Device {
  brandCode: string,
  code: string,
  name: string,
  film: string,
  cardboard: string,
  shape: string,
  clamshell: string
}

export interface DeviceCategory {
  brandName: string;
  brandCode: string;
  deviceName: string;
  deviceCode: string;
  code: string,
  name: string
}

export interface Color {
  code: string,
  name: string,
  colorHex: string
}

export interface CommonResponse {
  ok: boolean
  data: unknown
  message?: string
}

export interface ColorCategory {
  name: string;
  colorHex: string;
  code: string;
}

export interface PackCategory {
  name: string;
  code: string;
}

export interface CaseType {
  code: string,
  type: string,
  pack: string
}

export interface DesignSort {
  id: number,
  type: 'design-phone' | 'person-phone' | 'design-airpods';
  name: string;
  sorts: Array<string>;
  iconPath: string;
}

export interface RuntimeDesignSort {
  type: string,
  name: string,
  count: number
}

export interface DoneShipmentItem {
  id: number;
  shipmentId: number;
  status: string;
  caseTypeCode: string;
  quantity: number;
  allocatedQuantity: number;
  printedQuantity: number;
  packedQuantity: number;
}

export interface PrintingBatchShipmentItem {
  id: number;
  shipmentId: number;
  brandCode: string;
  deviceCode: string;
  colorCode: string;
}

export interface Printer {
  id: number,
  name: string,
  shortName: string,
  type: string
}

export interface Sticker {
  name: string,
  // eslint-disable-next-line camelcase
  case_type_code: string,
  designType: string,
  stickerImagePath: string,
  positionImagePath: string
}
