import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'

import NgV2 from '../views/NgV2.vue'
import AirPods from '../views/AirPods.vue'
import SolidSuit from '../views/SolidSuit.vue'
import ModNX from '../views/ModNX.vue'
import Hotstamp from '../views/Hotstamp.vue'
import Stock from '../views/Stock.vue'
import Sort from '../views/Sort.vue'
import MatchCase from '../views/MatchCase.vue'
import ClearCase from '../views/ClearCase.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/ng',
    name: 'ng-page',
    component: NgV2
  },
  {
    path: '/solidsuit',
    name: 'solidsuit-page',
    component: SolidSuit
  },
  {
    path: '/modnx',
    name: 'modnx-page',
    component: ModNX
  },
  {
    path: '/airpods',
    name: 'airpods-page',
    component: AirPods
  },
  {
    path: '/hotstamp',
    name: 'hotstamp-page',
    component: Hotstamp
  },
  {
    path: '/clear',
    name: 'clear-page',
    component: ClearCase
  },
  {
    path: '/stock',
    name: 'stock-page',
    component: Stock
  },
  {
    path: '/sort',
    name: 'sort-page',
    component: Sort
  },
  {
    path: '/match-case',
    name: 'match-case-page',
    component: MatchCase
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
