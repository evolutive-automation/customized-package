module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  pluginOptions: {
    electronBuilder: {
      nodeIntegration: true,
      builderOptions: {
        extraFiles: {
          from: "node_modules/pdf-to-printer/dist/SumatraPDF.exe",
          to: "./resources/app.asar.unpacked/SumatraPDF.exe"
        }
      }
    }
  }
}
